#[derive(PartialEq)]
pub enum SelectedTransactionList {
    PendingTxs,
    CompletedTxs,
}
